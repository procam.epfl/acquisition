import picamera
import adafruit_mlx90640
import time,board,busio
import numpy as np
import cv2

############################# DESCRIPTION #############################
"""
Upgraded version of the previewer to get more responsiveness (at the 
cost of not knowing the temperatures directly on display).
See dual_preview.py Description for more details.
"""
################################ CODE ################################

i2c = busio.I2C(board.SCL, board.SDA, frequency=400000) # setup I2C
mlx = adafruit_mlx90640.MLX90640(i2c) # begin MLX90640 with I2C comm
mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_16_HZ # set refresh rate

with picamera.PiCamera() as camera:
    w1 = 320
    h1 = 240
    w2 = 32
    h2 = 24

    # Cam init
    camera.resolution = (w1, h1)
    camera.framerate = 8
    
    
    mlx_shape = (h2,w2)
    
    print("LOADING...")
    time.sleep(2)
    vi_frame = np.empty((h1 * w1 * 3,), dtype=np.uint8)
    
    
     # INTER_LINEAR, INTER_AREA, INTER_CUBIC, INTER_NEAREST, INTER_LANCZOS4
        
    interpolation = cv2.INTER_LANCZOS4
#     for i in range(int(camera.framerate*10)):
    while True:
        #VISUAL
        camera.capture(vi_frame, 'rgb')
        vi_frame = vi_frame.reshape((h1, w1, 3))
        
        
        #THERMAL
        try:
            
            image2 = np.zeros((h2*w2,)) # setup array for storing all 768 temperatures
            mlx.getFrame(image2) # read MLX temperatures into frame var
            image2 = (np.reshape(image2,mlx_shape)) # reshape to 24x32
        
        except ValueError:
            print("error")
            continue # if error, just read again
        
        th_min = np.amin(image2)
        th_max = np.amax(image2)
        th_frame = np.zeros(shape=(h2, w2, 3), dtype=np.uint8)
        for y in range(image2.shape[0]):
            for x in range(image2.shape[1]):
                val = max(0,min(255,int((image2[y,x] - th_min) * 255 / (th_max - th_min) )))
                
                #CUSTOM FALSE COLORS :
                th_frame[y,x,0] = min(255,(max(val,122)-122)*255/123)
                th_frame[y,x,1] = abs(122-abs(val-122))*255/122
                th_frame[y,x,2] = (122 - min(val,122))*255/122
                #GRAYSCALE :
#                 th_frame[y,x,0] = val
#                 th_frame[y,x,1] = val
#                 th_frame[y,x,2] = val
        th_frame = cv2.cvtColor(cv2.flip(th_frame,0), cv2.COLOR_BGR2RGB)
        vi_frame = cv2.cvtColor(vi_frame, cv2.COLOR_BGR2RGB)
        th_frame = cv2.resize(th_frame, (w2*13, h2*13), interpolation=interpolation)
        resized_4_3 = (480, 360)
        image = cv2.hconcat((cv2.resize(np.rot90(vi_frame,2),resized_4_3), cv2.resize(th_frame,resized_4_3)))
        
        cv2.imshow("Preview", image)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    

cv2.destroyAllWindows()
