import os
import subprocess
from signal import *
import procam_data_namer
import time
import sys

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Attention !!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
#                                                                     #
# You should avoid running this program via an IDE, it                #
# it sometimes doesn't consider subprocessing the same                #
# as if you would use a terminal, the best is to write                #
# here the good parameters and to run it from the terminal            #
#                                                                     #
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#

############################# DESCRIPTION #############################
"""
This file is used to record dual content (thermal + visual).
It launches two subprocesses, running content from mlx_recorder and 
picamera_recorder. 

The main variables to control execution are :
- start_at : particularly the value you add to the current time, it is 
             used to be sure the subprocesses are synchronized
- video_time : the duration you want to record

You will also see that there are other important lines like, the one where 
we defined:
- th_filename : you specify information that will lead to create the 
unique_id, the fps, and the resolution.
- vi_filename : you create the filename based on the first one to avoid 
giving twice information, you can specify what you want to change though
More details about that at procam_data_namer.py
The point is that defining the filename also define properties of record.
"""
################################ CODE ################################


def hardclean(*args):
    global th_process
    global vi_process
    th_process.send_signal(SIGINT)
    vi_process.send_signal(SIGINT)
    print("Subprocesses should have been killed")
    sys.exit()
#     os.exit()
    

# TODO: Idea, to think to add processor temperature record over time, way of doing it by bash :
#      vcgencmd measure_temp | egrep -o '[0-9]*\.[0-9]*'
#      gives the temperature (celcius degree) as a float

# TODO: Find an easier idea than using subprocesses, or make it better to handle potential error

fps = 8
start_at = time.time() + 4
video_time = 300
config_id = 0

th_filename = procam_data_namer.generateFilename(False, "Matthieu", "Verdet", fps, (32,24), "rth")
vi_filename = procam_data_namer.generateFilenameFrom(th_filename, framerate=fps, resolution=(1440, 1080), extension="h264")

th_cmd = "python3 mlx_recorder.py %s %s %s" % (th_filename, start_at, video_time)
vi_cmd = "python3 picamera_recorder.py %s %s %s %s" % (vi_filename, start_at, video_time, config_id)

th_process = subprocess.Popen(th_cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
vi_process = subprocess.Popen(vi_cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)

try:
    print("Capture should start in ",round(start_at - time.time(),2)," seconds !")
    time.sleep(max((start_at - time.time())*0.95, 0.0001))
    print("NOW !")

    th_output = th_process.communicate()[0]
    vi_output = vi_process.communicate()[0]
except:
    print("Interrupted")
    hardclean()
    
print("DONE !")
print(str(vi_output))
print(str(th_output))



