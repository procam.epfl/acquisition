from mlx_raw_codec import MLXRawCodec
import sys
import procam_data_namer
import time
from signal import *
import os

############################# DESCRIPTION #############################
"""
File meant to be launched by dual_recorder.py
Starts a thermal record using MLXRawCodec based on the variables 
filename, start_at, video_time provided by command line.

The filename provides the video properties returned by the 
method detailsFromFilename.
"""
################################ CODE ################################

def hardclean(*args):
    global mlxrc
    global filename
    
    mlxrc.close()
    os.remove(filename)
    print("Thermal Recording Failed !")
    sys.exit()
#     os.exit()

for sig in (SIGILL, SIGINT, SIGPIPE, SIGTERM):
        signal(sig, hardclean)

filename = sys.argv[1]

details = procam_data_namer.detailsFromFilename(filename)


start_at = float(sys.argv[2])
video_time = float(sys.argv[3])


mlxrc = MLXRawCodec(filepath=filename, image_width = details["width"],
                    image_height = details["height"], fps=details["framerate"])



mlxrc.encode(video_time, start_at)
mlxrc.close()


print("Thermal Recording Done !")
