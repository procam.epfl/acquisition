import adafruit_mlx90640
import time,board,busio
import numpy as np
import cv2
import matplotlib.pyplot as plt
import io
import sys

encode = False;
if encode:
    # Header Infos
    w = 32
    h = 24
    f = 16

    file = open("raw_thermal_video_test.rthv","wb")
    header = np.array([w,h,f], dtype=np.uint8)
    file.write(bytearray(header))

    print("CAMERA SETUP...")
    i2c = busio.I2C(board.SCL, board.SDA, frequency=400000) # setup I2C
    mlx = adafruit_mlx90640.MLX90640(i2c) # begin MLX90640 with I2C comm
    mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_16_HZ # set refresh rate

    frame = np.zeros((h*w,))

    #     for i in range(int(camera.framerate*10)):
    print("RUNNING...")
    video_time = 5 # seconds
    # while True:
    for t in range(int(video_time*f)):
        #THERMAL
        try:
             # setup array for storing all 768 temperatures
            mlx.getFrame(frame) # read MLX temperatures into frame var
            file.write(bytearray(frame))
        except ValueError:
            print("error")
            continue # if error, just read again
        
    
    print("CLOSING FILE...")
    file.close()
else:
    # sys.argv[1]
    file = open("raw_thermal_video_test.rthv","rb")

    # Header Infos
    header = np.array(list(file.read(3)), np.uint8)
    w = int(header[0])
    h = int(header[1])
    f = int(header[2])

    mlx_shape = (h,w)
    frame_size = h*w

    plt.ion()
    fig = plt.figure(figsize=(w,h))
    show = plt.imshow(np.zeros((h, w)), animated=True)
    cbar = fig.colorbar(show) # setup colorbar for temps
    cbar.set_label('Temperature [$^{\circ}$C]',fontsize=14) # colorbar label

    frame = np.array(np.fromfile(file, dtype=np.float64, count=frame_size), np.float64)
    
    #     for i in range(int(camera.framerate*10)):
    print("RUNNING...")
    
    while len(frame) > 0:
        #THERMAL
        image = (np.reshape(frame, mlx_shape)) # reshape to 24x32
        
        show.set_data(np.rot90(np.fliplr(image),2)) # flip left to right
        show.set_clim(vmin=np.min(image),vmax=np.max(image)) # set bounds
        
        cbar.on_mappable_changed(show) # update colorbar range
        
        # Since reading take a lot of time, we assume we can double
        # the frequency just for debug display (it could go slower, but not faster)
        plt.pause(0.5/f)
        
        frame = np.array(np.fromfile(file, dtype=np.float64, count=frame_size), np.float64)

    print("CLOSING FILE...")
    file.close()





