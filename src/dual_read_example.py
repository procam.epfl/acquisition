import time
import numpy as np
import cv2
import matplotlib.pyplot as plt
from mlx_raw_codec import MLXRawCodec
import sys

import procam_data_namer as namer

############################# DESCRIPTION #############################
"""
This (slightly confusing) code is an example of how you can use 
conjointly files from a single/dual record.
As explained in the data naming convention, each record possesses a 
unique id. If you select a valid one, the method findfile will help you 
retreving the whole filename, and the method detailsFromFilename
will give you elementary properties of the record based on the filename.

After that, we gather data from the files in arrays, rework the arrays 
to display something pleasing to our eye and perform any transformation
we would like. 

The important parts here is to show :
- how to retrieve filename based on the unique id
- how to load the data with cv2 or mlx_raw_codec
What is made with the data afterward is your job :D

(You can use it as record viewer, by setting up frame_start_index and 
frame_count_selection.
It might be not error-proof, it was not intensely tested)
"""
################################ CODE ################################


unique_id = "mattverd1"

direc = "data_examples/"
vi_filename = namer.findFile(is_visual=True, unique_id=unique_id, directory=direc)
th_filename = namer.findFile(is_visual=False, unique_id=unique_id,directory=direc)

framerate = namer.detailsFromFilename(th_filename)["framerate"]

vi_filename = direc + vi_filename
th_filename = direc + th_filename

# --------------------------------------------#
# VISUAL LOADING
# --------------------------------------------#
frame_start_index = 50
frame_count_selection = 80

cap=cv2.VideoCapture(vi_filename)

vi_frames = []
k = 0
ret, vi_frame = cap.read()
while cap.isOpened() and ret and k < frame_start_index + frame_count_selection:
    if k >= frame_start_index:
        vi_frame = np.rot90(vi_frame,k=2,axes=(0,1))
        vi_frames.append(vi_frame)
    ret, vi_frame = cap.read()
    k+=1
    
cap.release()

# --------------------------------------------#
# THERMAL LOADING
# --------------------------------------------#

th_codec = MLXRawCodec(th_filename, decode=True)

th_raw_frames = th_codec.decode(frame_index=frame_start_index, decoded_frame_count=frame_count_selection)

# Convert them to color :

th_w = th_codec.getImageWidth()
th_h = th_codec.getImageHeight()
# interpolation = cv2.INTER_LINEAR
# interpolation = cv2.INTER_AREA
# interpolation = cv2.INTER_NEAREST
interpolation = cv2.INTER_CUBIC
# interpolation = cv2.INTER_LANCZOS4
th_frames = []
for th_raw_frame in th_raw_frames:
    th_min = np.amin(th_raw_frame)
    th_max = np.amax(th_raw_frame)
    th_frame = np.zeros(shape=(th_h, th_w, 3), dtype=np.uint8)
    for y in range(th_raw_frame.shape[0]):
        for x in range(th_raw_frame.shape[1]):
            th_frame[y,x,0] = int((th_raw_frame[y,x] - th_min) / (th_max - th_min) * 255)
            th_frame[y,x,1] = th_frame[y,x,0]
            th_frame[y,x,2] = th_frame[y,x,0]
    th_frame = cv2.resize(np.rot90(th_frame, 2), (th_w*30, th_h*30), interpolation=interpolation)
    th_frames.append(th_frame)


# --------------------------------------------#
# CONCAT
# --------------------------------------------#
concat_frames = []
resized_16_9 = (640, 360)
resized_4_3 = (480, 360)
for i in range(min(len(th_frames), len(vi_frames))):
    concat_frames.append(cv2.hconcat((cv2.resize(vi_frames[i],resized_4_3), cv2.resize(th_frames[i],resized_4_3))))
    

# --------------------------------------------#
# SHOW
# --------------------------------------------#

# plt.ion()
# fig = plt.figure(figsize=(8,8), dpi=20)
i = 0
# show = plt.imshow(frames[0])

show_start = time.time()
last_showing_time = show_start
while i < min(len(th_frames), len(vi_frames)):
#     show.set_data(frames[i])
    cv2.imshow("test",concat_frames[i])
#     cv2.imshow("test",th_frames[i])
#     cv2.imshow("test",vi_frames[i])
        
    pause_time = show_start + i/framerate - time.time() - 0.0001
    
    if cv2.waitKey(max(int(pause_time*1000), 1)) & 0xFF == ord('q'):
        break
    
#          plt.pause(max(pause_time, 0.0001))
#          plt.pause(0.0001)
        
    if pause_time < 0.0001:
        show_start -= pause_time
    
    print(1/(time.time() - last_showing_time))
    i+=1
    last_showing_time = time.time()
 
print("Visual array length : ", len(vi_frames))
print("Thermal array length : ", len(th_frames))
cv2.destroyAllWindows()
