import time
import numpy as np
import cv2
import io

############################# DESCRIPTION #############################
"""
Class defining an encoder/recorder for thermal content captured 
by MLX90640 thermal camera. 
Additionnally there are many examples of how to use it below.

--- HEADER ---

MLXRawCodec

    __filepath
    __ready
    __image_width
    __image_height
    __fps
    __frame_count
    verbose

    __init__(self, filepath, decode=False, verbose=False, **kwargs[image_width, image_height, fps])
    
    encode(self, video_time = 5, start_at = 0)
    decode(self, frame_index = 0, decoded_frame_count = -1, is_plotting=False)
    
    setImageWidth(self, new_image_width)
    getImageHeight(self)
    
    setImageHeight(self, new_image_height)
    getImageWidth(self)
    
    setFPS(self, new_fps)
    getFPS(self)
    
    __getHeader(self)
    __setHeader(self, video_time)
    
    close(self)
"""
################################ CODE ################################

class MLXRawCodec:
    """
    Class defining an encoder/recorder for thermal content captured 
    by MLX90640 thermal camera.
    """
    FPS_ACCEPTED_VALUES = [0.5, 1, 2, 4, 8, 16, 32]
    HEADER_SIZE = 7
    
    """
    - @filepath : describes the path of the filename we want to create/read
    - @decode : if False, determines that we instantiate the class by 
            creating/overwriting a file based on filepath. If True, 
            determines that we instantiate the class by reading a file 
            based on filepath. We still have the right to over-write over 
            it afterward, but it   will initially remained untouched.
    - @verbose : display the printings
    - @image_width, @image_height, @fps : optional values used when decode=False. 
            image_width is at max 32, image_height is at max 24, and fps should
            be one of this value : 0.5, 1, 2, 4, 8, 16, 32
            When decode=True it will setup the values of the codec from the file.
    """
    def __init__(self, filepath, decode=False, verbose=False, **kwargs):
        self.__filepath = filepath
        self.__ready = False
        self.__image_width = 0
        self.__image_height = 0
        self.__fps = 0
        self.__frame_count = np.uint32(0)
        self.verbose = verbose
        if decode:
            self.__file = open(self.__filepath, "rb+")
            self.__getHeader()
            
        else:
            self.__file = open(self.__filepath, "wb+")
            
            self.setImageWidth(kwargs.get("image_width", 32))
            self.setImageHeight(kwargs.get("image_height", 24))
            self.setFPS(kwargs.get("fps", 8))
            
            self.__file.write(bytes(self.__frame_count))
            
                      
    """
    Always call setHeader(self, video_time) to make sure the header is well defined with 
    fresh attributes and the file cursor is well positioned. Start capturing all the frames 
    at at_start time, store them in RAM, and encode them directly after the header in the file.

    - @video_time : the duration of the video we wish to record (will define the framecount based on 
            the fps attribute). Setting video_time = 0 means taking a single photo.
    - @start_at : the OS time at which we want to start recording (helps for synchronization later)
    """
    def encode(self, video_time = 5, start_at = 0):
        import adafruit_mlx90640
        import board,busio
        
        self.__setHeader(video_time)
        
        captured_frames = np.zeros((self.__frame_count, self.__image_height * self.__image_width), dtype=np.float64)
        
        ####################################
        # PREPARE THE CAPTURE
        ####################################
        
        print("CAMERA SETUP...")
        i2c = busio.I2C(board.SCL, board.SDA, frequency=400000) # setup I2C
        mlx = adafruit_mlx90640.MLX90640(i2c) # begin MLX90640 with I2C comm
        mlx.refresh_rate = self.__mlx_fps # set refresh rate
        
        fresh_frame = np.zeros((self.__image_height*self.__image_width,))
        frame = fresh_frame
        
        if start_at > 0 :
            time.sleep(max(0.001, start_at - time.time()))
        
        print("RUNNING...")
        last_capture_time = time.time()
        elapsed_capture_time = time.time()
        
        for f in range(self.__frame_count):
            
            try:
                mlx.getFrame(fresh_frame) # read MLX temperatures into frame var
                if self.verbose:
                    print("Freq : ", round(1/(time.time() - last_capture_time),2))
                last_capture_time = time.time()
                frame = fresh_frame
                captured_frames[f] = frame
            except ValueError:
                print("error")
                # TODO: determine what to do when an error occurs. For the moment write again the last frame
                captured_frames[f] = frame
                continue
            
        print("Total Time vs Required Time : ", time.time() - elapsed_capture_time, " | ", video_time)
        print("SAVING...")
        for f in range(self.__frame_count):
            self.__file.write(bytearray(captured_frames[f]))
        print("DONE !")
    
       # TODO: Associate a sliding buffer with the class, to reduce the access time.
    
    """
    Calls getHeader(...) if ready is False. And returns an array of 32x24 float64 frames if 
    is_plotting is False decoded from the file. None otherwise.

    - @frame_index : the first frame we wish to decode
    - @decoded_frame_count : the amount of frames we want to decode
    - @is_plotting : implies to visualize directly the data via matplotlib if True
    """
    def decode(self, frame_index=0, decoded_frame_count=-1, is_plotting=False):
        if not self.__ready:
            self.__getHeader()
        
        mlx_shape = (self.__image_height, self.__image_width)
        frame_size = int(self.__image_height)*int(self.__image_width)
        
        frame_index = frame_index % self.__frame_count
        
        self.__file.seek(int(self.HEADER_SIZE + frame_index * frame_size * 8)) # 8 is for the size of float64
        
        if decoded_frame_count < 0 or frame_index + decoded_frame_count > self.__frame_count:
            decoded_frame_count = self.__frame_count - frame_index
        elif decoded_frame_count == 0 :
            print("You requested to decode 0 frame")
            return None

        if is_plotting:
            import matplotlib.pyplot as plt
            plt.ion()
            
            fig = plt.figure(figsize=(8,8), dpi=50)
            show = plt.imshow(np.zeros(mlx_shape), animated=True)
            cbar = fig.colorbar(show) # setup colorbar for temps
            cbar.set_label('Temperature [$^{\circ}$C]',fontsize=14) # colorbar
            show.set_clim(vmin=20,vmax=40)
            cbar.on_mappable_changed(show)
        else :
            frame_result = np.zeros((int(decoded_frame_count), self.__image_height, self.__image_width), dtype=np.float64)
            
        frame = np.fromfile(self.__file, dtype=np.float64, count=frame_size)
        
        print("RUNNING...")
        frame_iteration = 0
        show_start = time.time()
        last_showing_time = show_start
        while len(frame) > 0 and frame_iteration < decoded_frame_count:
            image = np.fliplr(np.reshape(frame, mlx_shape)) # reshape to 24x32
            
            if is_plotting:
                
                show.set_data(np.rot90(image,2))
                # show.set_clim(vmin=np.min(image),vmax=np.max(image))
                # cbar.on_mappable_changed(show)
                pause_time = show_start + (frame_iteration)/self.__fps - time.time() - 0.0001
                last_showing_time = time.time()
                plt.pause(max(pause_time, 0.0001))
                if pause_time < 0.0001:
                    show_start -= pause_time
                if self.verbose:
                    print(1/(time.time() - last_showing_time))
                
                
            else:
                frame_result[frame_iteration] = image
            # Loading next Frame
            frame_iteration += 1
            frame = np.fromfile(self.__file, dtype=np.float64, count=frame_size)
            
        if is_plotting:
            return None
        else:
            return frame_result
    
    """
    Checks new_image_width validity and clamp it to [1;32] if wrong
    and set ready to False.  
    """
    def setImageWidth(self, new_image_width):
        self.__image_width = max(1,min(32,new_image_width))
        self.__ready = False
    
    
    def getImageHeight(self):
        return self.__image_height
    
    """
    Checks new_image_height validity and clamp it to [1;24] if wrong
    and set ready to False.  
    """
    def setImageHeight(self, new_image_height):
        self.__image_height = max(1,min(24,new_image_height))
        self.__ready = False
    
    
    def getImageWidth(self):
        return self.__image_width
    """
    Checks new_fps validity, update __fps, the practical framerate of the MLX __mlx_fps
    and set ready to False.  
    """
    def setFPS(self, new_fps):
        
        self.__fps = new_fps
        try :
            # Apparently run at half the fps so, little hack:
            self.__mlx_fps = self.FPS_ACCEPTED_VALUES.index(self.__fps*2)
        except ValueError as e:
            print(type(e))
            print(e.args)
            print(e)
            print("The value \"fps\" should be in this list :\n", self.FPS_ACCEPTED_VALUES)
        self.__ready = False
            
    def getFPS(self):
        return self.__fps
    
    """
    Based on __filepath provided at initialization, will look at the header of the file and 
    accordingly set all the properties (__image_width, __image_height, __fps, __framecount) from 
    it and set __ready to True.
    """
    def __getHeader(self):
        self.__file.seek(0)
        header = np.fromfile(self.__file, dtype=np.uint8, count=3)
        
        self.__image_width = header[0]
        self.__image_height = header[1]
        self.setFPS(header[2])
        self.__frame_count = np.fromfile(self.__file, dtype=np.uint32, count=1)
        self.__ready = True
    
    """
    Based on the provided video_time and the attributes it already has (from initialization or 
    from setters), rewrites the header in the file, put the file cursor ready to write frames, 
    and set __ready to True
    """
    def __setHeader(self, video_time):
        if not self.__ready:
            self.__file.seek(0)
            
            header = np.array([self.__image_width,self.__image_height,self.__fps], dtype=np.uint8)
            self.__file.write(bytearray(header))
        else :
            self.__file.seek(3)
            
        if video_time == 0:
            self.__frame_count = np.uint32(1)
        elif video_time > 0 :
            self.__frame_count = np.uint32(round(video_time*self.__fps))
        else:   
            raise Exception("video_time")
        
        self.__file.write(self.__frame_count)
        self.__ready = True

    def close(self):
        self.__file.close()

# ---------------------------------------------------------------------------- #

# RECORDING EXAMPLE
# test = MLXRawCodec(filepath="raw_thermal_video_test.rth", fps = 4)
# test.encode(3) # take a video of 3 seconds
# # test.encode(0) # take a photo
# test.close()

# ---------------------------------------------------------------------------- #

# VIEWING EXAMPLE 1
# test = MLXRawCodec(filepath="raw_thermal_video_test.rth", decode=True)
# test.decode(is_plotting = True)
# test.close()

# ---------------------------------------------------------------------------- #

# VIEWING EXAMPLE 2
# test = MLXRawCodec(filepath="raw_thermal_video_test.rth", decode=True)
# subvideo = test.decode(frame_index = 32, decoded_frame_count = 8)
# 
# fig, axes = plt.subplots(2,4, figsize=(32,24))
# 
# for i in range (8):
#     axes[i//4,i%4].imshow(subvideo[i])
# plt.show()    
# test.close()

# ---------------------------------------------------------------------------- #

# RECORDING & REVIEWING EXAMPLE 1
# test = MLXRawCodec(filepath="raw_thermal_video_longtest.rth")
# test.encode(60) # take a video of 3 seconds
# start_time = 20
# watch_time = 5
# test.decode(frame_index = test.getFPS()*start_time, decoded_frame_count=watch_time*test.getFPS(), is_plotting=True)
# test.close()

# ---------------------------------------------------------------------------- #

# RECORDING & REVIEWING EXAMPLE 2
# test = MLXRawCodec(filepath="raw_thermal_video_test1.rth", verbose=True, fps=8)
# test.encode(5, start_at=time.time()+3)
# test.decode(is_plotting=True)
# test.close()

# ---------------------------------------------------------------------------- #

# RECORDING & REVIEWING EXAMPLE 3
# test = MLXRawCodec(filepath="raw_thermal_photo_test0.rth")
# test.encode(0)
# test.decode(is_plotting=True)
# test.close()

# ---------------------------------------------------------------------------- #

# del test
