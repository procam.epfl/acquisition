import picamera
import adafruit_mlx90640
import time,board,busio
import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

############################# DESCRIPTION #############################
"""
This file is meant to be a simplistic previewer to get a 4:3 livefeed 
at 8 frames per second of both visual and thermal camera. 
It uses matplotlib to show the temperature.
Since it's not super performant, I created another one using cv2 
lib to show a more reactive result. It's basically the same code though. 
"""
################################ CODE ################################

i2c = busio.I2C(board.SCL, board.SDA, frequency=400000) # setup I2C
mlx = adafruit_mlx90640.MLX90640(i2c) # begin MLX90640 with I2C comm
mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_16_HZ # set refresh rate

with picamera.PiCamera() as camera:
    w1 = 320
    h1 = 240
    w2 = 32
    h2 = 24
    plt.ion()
    fig, (ax1, ax2) = plt.subplots(1, 2,figsize=(w1/10,h1/10))
    # Cam init
    camera.resolution = (w1, h1)
    camera.framerate = 16
    
    mlx_shape = (h2,w2)
    
    show1 = ax1.imshow(np.zeros((h1, w1)), animated=True)
    show2 = ax2.imshow(np.zeros((h2, w2)), animated=True)
    print("LOADING...")
    time.sleep(2)
    image1 = np.empty((h1 * w1 * 3,), dtype=np.uint8)
    
    cbar = fig.colorbar(show2) # setup colorbar for temps
    cbar.set_label('Temperature [$^{\circ}$C]',fontsize=14) # colorbar label

    
    
#     for i in range(int(camera.framerate*10)):
    while True:
        #VISUAL
        camera.capture(image1, 'rgb')
        image1 = image1.reshape((h1, w1, 3))
        show1.set_data(np.rot90(image1, 2))
        
        #THERMAL
        try:
            image2 = np.zeros((h2*w2,)) # setup array for storing all 768 temperatures
            mlx.getFrame(image2) # read MLX temperatures into frame var
            image2 = (np.reshape(image2,mlx_shape)) # reshape to 24x32
        
            show2.set_data(np.rot90(np.fliplr(image2),2)) # flip left to right
            show2.set_clim(vmin=np.min(image2),vmax=np.max(image2)) # set bounds
        
            cbar.on_mappable_changed(show2) # update colorbar range
        except ValueError:
            print("error")
            continue # if error, just read again
        
        plt.pause(0.001)


