import picamera
import time
import procam_data_namer
import sys
from signal import *
import os

############################# DESCRIPTION #############################
"""
File meant to be launched by dual_recorder.py
Starts a visual record using picamera based on the variables `filename` 
(this time analogous to `vi_filename`), `start_at` and  `video_time` in 
the exact same fashion as for thermal record.

When the resolution is set to high, or the framerate is 
conjointly with resolution asking for a too high bitrate, the dual 
record can fail and break. Settings have to be experimentally verified.

After changing the resolution to 1440x1080 and 
approximatively setting a bitrate (based on framerate and resolution), 
we get much more reliable results, in terms of crash avoiding and data
 weight scalability (before 8 minutes of splitted visual data = around 
 1 GB, now 5 minutes of continuous record = around 100 MB) 
"""
################################ CODE ################################

def hardclean(*args):
    global filename
    global camera
    camera.close()
    os.remove(filename)
    print("Visual Recording Failed !")
    sys.exit()
#     os.exit()

for sig in (SIGILL, SIGPIPE, SIGINT, SIGTERM):
        signal(sig, hardclean)
camera = picamera.PiCamera()
try:
    
    filename = sys.argv[1]
    
    
    details = procam_data_namer.detailsFromFilename(filename)
    
    start_at = float(sys.argv[2])
    video_time = float(sys.argv[3])
    pre_config_id = int(sys.argv[4])
    # max_resolution = (2560, 1920)
    camera.framerate = details["framerate"]

    camera.resolution = details["resolution"]
    
    #TODO: map pre_config_id to a set of camera_settings :
        # camera.brightness = 70
        # camera.contrast = 5

        # camera.image_effect = 
        # camera.IMAGE_EFFECT
        # none, negative, solarize, sketch, denoise, emboss, oilpaint, hatch,
        # gpen, pastel, watercolor, film, blur, saturation, colorswap, washedout,
        # posterise, colorpoint, colorbalance, cartoon, deinterlace1, deinterlace2

        # camera.exposure_mode =
        # camera.EXPOSURE_MODES
        # off, auto, night, nightpreview, backlight, spotlight, sports, snow, beach,
        # verylong, fixedfps, antishake, fireworks

        # camera.awb_mode =
        # camera.AWB_MODES
        # off, auto, sunlight, cloudy, shade, tungsten, fluorescent, incandescent, flash, horizon
        
        # OR SEE DOC
        
    # Bitrate approximation :
    # Weight in bits of one second of recording
    bitrate = details["width"]*details["height"]*details["framerate"]*3*8
    bitrate *=0.02 # Wanted Compression
    
    #Rounding the value
    if bitrate > 100000:
        bitrate = int(bitrate/100000)*100000
    else:
        bitrate = 100000
        
    time.sleep(max(0.001, start_at - time.time()))
    
    #TODO: Make better compression settings
    #https://picamera.readthedocs.io/en/release-1.9/api.html#picamera.PiCamera.start_recording
    camera.start_recording(filename, bitrate=bitrate)
    camera.wait_recording(video_time)
    
    camera.stop_recording()
except:
    sys.exit()
finally:
    camera.close()

print("Visual Recording Done !")
