import os

from parse import *
############################# DESCRIPTION #############################
"""

 HEADER:
    generateFilename(is_visual=False, name="Name", surname="Surname", framerate="8", 
            resolution=(32,24), extension = None, record_number = None, 
            directory = os.getcwd(), details=None):
    detailsFromFilename(filename)
    generateFilenameFrom(filename, framerate=None, resolution=None, extension=None)
    findFile(is_visual, unique_id, directory=os.getcwd())
"""
################################ CODE ################################

UNIQUE_ID_STRING_PART_SIZE = 8
UNIQUE_ID_NAME_PART_SIZE = 4


"""
Generates a filename as a String based on all these properties :
- @is_visual : if True, the filename starts with "visual_...", otherwise"thermal_..." .
- @name, surname : together they defined the first part of the unique_id, it takes the K (4 or less)
         first characters of your name, and completes with the 8-K (or less) first characters of 
         your surname. If the total doesn’t reach 8 characters, we pad up with # : the overall 
         gives us the signature.
         If record_number is provided, then we concatenate the record_number value at the end of the 
         signature, this gives us the unique_id. 
         Otherwise, it means we want to look in directory to see if we have other files with the same 
         signature. If it’s the case then we look for the highest record_number found in those and 
         increment by one, then as before we concatenate this new record number to the signature to 
         form the unique id.
- @framerate : a float indicating the framerate of the video. In the string : "..._f<framerate>..."
- @resolution : a pair of integer indicating the resolution of the frames of the video. In the string : 
         "..._r<resolution[0]>x>resolution[1]..."
- @extension : a string indicating the extension file of the data. At the end of the string : 
         ".<extension>"
- @directory : the directory where to look at when looking for unique_id
- @details : a dictionary principally used in generateFilenameFrom(...). If not None, generates 
         filename only based on the properties of the details dictionary.

Side-Note : the default values are based on the thermal version of filename, since those doesn’t change that much.
"""
def generateFilename(is_visual=False, name="Name", surname="Surname", framerate=8, resolution=(32,24), extension = None, record_number = None, directory = os.getcwd(), details=None):
    # Dictionary Case :
    if details:
        resolution = details['resolution']
        if 'extension' in details:
            extension = details['extension']
        else:
            extension = None
            
        if details['video_type'] == "thermal" or details['video_type'] == "visual" :
            is_visual = details['video_type'] == "visual"
        else :
            print("You gave a details dictionary with incompatible video_type")
            return None
        framerate = details['framerate']
        
        unique_id = details["unique_id"]
    else :
        name_sublength = min(UNIQUE_ID_NAME_PART_SIZE,len(name))
        surname_sublength = min(UNIQUE_ID_STRING_PART_SIZE-name_sublength, len(surname))
        filling_length = max(UNIQUE_ID_STRING_PART_SIZE - name_sublength - surname_sublength, 0)
        unique_id = (name[0:name_sublength] + surname[0:surname_sublength] + ("#" * filling_length)).lower()
        if record_number:
            unique_id += str(record_number)
        else :
            max_record_number = 0
            for entry in os.listdir(directory):
                if os.path.isfile(os.path.join(directory, entry)):
#                     print(entry)
                    details = detailsFromFilename(entry)
                    
                    if details:
                        max_record_number = max(max_record_number, details["record_number"])
            unique_id += str(max_record_number + 1)
        
    filename = ""
    if is_visual:
        filename += "visual_"
    else:
        filename += "thermal_"
        
    if extension:
        extension = "." + extension
    else:
        extension = ""
        
    filename += unique_id + "_f" + str(framerate) + "_r" + str(resolution[0]) + "x" + str(resolution[1]) + extension
    
    return filename

def detailsFromFilename(filename):
    """Function that returns the details contained in the given filename

    The returned dictionary is composed of the main elements :
        - 'video_type' giving the type "thermal" or "visual" of the video as a string
        - 'unique_id' of the record (dual or not) as a string
        - 'framerate' as a float
        - 'resolution' as a pair
        - 'extension' if it exists, as a string
    Other secondary values :
        - 'record_number' of the record (the last part of the unique ID)
        - 'width' which is first element of the resolution pair
        - 'height' which is second element of the resolution pair
    """
    p = parse("{details[video_type]}_{details[unique_id]}_f{details[framerate]}_r{details[width]}x{details[height]}.{details[extension]}", filename)
    if not p:
        p = parse("{details[video_type]}_{details[unique_id]}_f{details[framerate]}_r{details[width]}x{details[height]}", filename)
        if not p:
            return None
        
    details = p['details']
    
    if details['video_type'] != "thermal" and details['video_type'] != "visual":
        print(details)
        print("Be careful, your video type is not correct !!!!")
        return None
    
    details['framerate'] = float(details['framerate'])
    details['width'] = int(details['width'])
    details['height'] = int(details['height'])
    
    details['resolution'] = (details['width'],details['height'])
    details['record_number'] = int(details['unique_id'][8:])
    
#     print(details)
    return details
"""
This method is a time-saver when we want to define both thermal and visual filename. We define the first one 
with generateFilename(...) (generally the thermal one), and we can get the second one (respectively, the visual) 
with this method, by providing the first filename and whether we want to change framerate, resolution. We have to 
change extension no matter what, since it’s defining the whole file format. In the same fashion, resolution often
has to be updated to match the ability of the targeted camera (relevantly higher for visual and always lower for thermal)
"""
def generateFilenameFrom(filename, framerate=None, resolution=None, extension=None):
    details = detailsFromFilename(filename)
    
    if details:
        if details['video_type'] == "thermal" or details['video_type'] == "visual" :
            if details['video_type'] == "thermal" :
                details['video_type'] = "visual"
            else:
                details['video_type'] = "thermal"
        if extension:
            details['extension'] = extension
        elif 'extension' in details:
            del details['extension']
        
        if framerate:
            details['framerate'] = framerate
        if resolution:
            details['resolution'] = resolution
            
        return generateFilename(details=details)
    else:
        return None

"""
Based on the unique_id and whether it is_visual, it will look in the given directory to check 
if a file match this description and will return the whole filename as a String if yes. Otherwise returns None
"""
def findFile(is_visual, unique_id, directory=os.getcwd()):
    for entry in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, entry)):
            details = detailsFromFilename(entry)
            if details['unique_id'] == unique_id:
                if (is_visual and details["video_type"] == "visual") or (not is_visual and details["video_type"] == "thermal"):
                    return entry
    print("No file with these details was found")
    return None
                
                    


# # Test to show basic use of the function
# th_f = generateFilename(False, "Matthieu", "Verdet", 8, (32,24), "rth")
# vi_f = generateFilenameFrom(th_f, framerate=32, resolution=(1920, 1080), extension="h264")
# 
# print(th_f)
# print(vi_f)
# # print(detailsFromFilename(th_f))

#     
# # Other test to show what happen if the addition of the length of both your name, surname
# vi_f2 = generateFilename(False, "Aaa", "Bbb", 13.5, (530,249))
# th_f2 = generateFilenameFrom(vi_f2, resolution=(32, 24))
# 
# print()
# print(vi_f2)
# print(th_f2)
