# ProCam Data-Set Acquisition 
All code linked to researches on acquisition or on the organisation/creation of the Data-Set

## Data Naming Conventions (`procam_data_namer.py`)
Based on the idea that we should have generalization of how we name our files [here](https://docs.google.com/spreadsheets/d/1gwADkyG-fqijfhUon4SxZQ5uz7Srnv4VCW0BmOuDWSA/edit?usp=sharing)

### `generateFilename(is_visual=False, name="Name", surname="Surname", framerate=8, resolution=(32,24), extension = None, record_number = None, directory = os.getcwd(), details=None)`
**Generates a filename** as a *String* based on all these properties : 
- `is_visual` : if `True`, the filename starts with `"visual_..."`, otherwise`"thermal_..."` .
- `name`, `surname` : **together** they defined the **first part** of the **`unique_id`**, it takes the *K* (*4 or less*) first characters of your `name`, and completes with the *8-K* (*or less*) first characters of your `surname`. If the total doesn't reach 8 characters, we pad up with `#` : the overall gives us the **`signature`**. 
If `record_number` is provided, then we concatenate the `record_number` value at the end of the **`signature`**, this gives us the **`unique_id`**
Otherwise, it means we want to look in `directory` to see if we have other files with the same **signature**. If it's the case then we look for the **highest** `record_number` found in those and **increment by one**, then *as before* we concatenate this new record number to the signature to form the unique id.

- `framerate` : a **float** indicating the **framerate of the video**. In the string : `"..._f<framerate>..."`
- `resolution` : a **pair of integer** indicating the **resolution of the frames** of the video. In the string : `"..._r<resolution[0]>x>resolution[1]..."` 
- `extension` : a **string** indicating the **extension file** of the data. At the end of the string : `".<extension>"`
- `directory` : the **directory where to look at** when looking for **`unique_id`**
- `details` : a **dictionary** principally used in `generateFilenameFrom(...)`. If not **None**, generates filename ***only* based on the properties of the `details` dictionary.**
> Side-Note : the default values are based on the thermal version of filename, since those doesn't change that much. 
### `detailsFromFilename(filename)`
Function that returns **the details** contained in the **given filename**
The **returned dictionary** is composed of the *main elements* :
- `video_type` giving the type **`"thermal"` or `"visual"`** of the video as a **string**
- `unique_id` of the record (dual or not) as a **string**
- `framerate` as a **float**
- `resolution` as a **pair**
- `extension` if it exists, as a **string**
Other *secondary values* :
- `record_number` of the record (the **last part of the unique ID**)
- `width` which is **first element** of the resolution pair
- `height` which is **second element** of the resolution pair

### `generateFilenameFrom(filename, framerate=None, resolution=None, extension=None)`
This method is a *time-saver* **when we want to define both thermal and visual filename**. We define the **first one** with `generateFilename(...)` (*generally the thermal one*), and we can get the **second one** (*respectively, the visual*) with this method, by providing the first `filename` and **whether we want to change `framerate`, `resolution`**. We **have to** change **`extension`** no matter what, since it's defining the whole file format. In the same fashion, *resolution often has to be updated to match the ability of the targeted camera* (relevantly higher for visual and always lower for thermal)

### `findFile(is_visual, unique_id, directory=os.getcwd())`
Based on the `unique_id` and whether it `is_visual`, it will look in the given `directory` to check if a file match this description and will return the whole filename as a **String** if yes. Otherwise returns **None**  

## Raw Thermal Codec (`mlx_raw_codec.py`)
Class defining an **encoder/recorder** for **thermal content** captured by **MLX90640 thermal camera** ([more details here](https://docs.google.com/document/d/1ZqwJtVHQwBIcsT2UFKf_JCseQgSN0RfqODXfO_9FJGs/edit?usp=sharing)).
There *was no library* to save thermal videos with the MLX90640, *this is a simple codec to allow that*. 
The class works as binary file writer/reader open, as long as you don't call the `close()` method you **can modify the file**.

It produces files with a **Header of 7 bytes** + all the frames one after the others :

|Elements|Size|
|--|--|
|Image Width |1 Byte|
|Image Height|1 Byte|
|Framerate|1 Byte|
|Framecount|4 Bytes|
|Frames... | Framecount x Image Width x Image Height x *8 Bytes*|

The *unit value* for an image is **float64**, this is where the *8 Bytes* comes from.

You instantiate the class with :
###  **`__init__(self, filepath, decode=False, verbose=False, **kwargs<image_width, image_height, fps>)`**
 - `filepath` : describes the **path** of the **filename** we want to **create/read**
 - `decode` : if `False`, determines that we instantiate the class by **creating/overwriting** a file based on `filepath`. If `True`, determines that we instantiate the class by **reading** a file based on `filepath`. We *still have the right to overwrite it* afterward, but it will initially remained untouched. 
 - `verbose` : displays the printings or not
 - `image_width`,`image_height`,`fps` : **optional values** used when `decode=False`.
  `image_width` is **at max 32**,  `image_height` is **at max 24**, and `fps` should be one of this value : **0.5, 1, 2, 4, 8, 16, 32**
 When `decode=True` it will **setup the attributes of the codec class** from the file.
> Side-Note : practically, I never got good result using fps over 8 without strongly changing firmware, or to allow huge hardware bitrate (which could cause overheat). Also Important to note that practically when we want N exact frames per second, we need to ask Nx2 Hz in the MLX library, this is handled internally, you just need to care about the real N value.

Before getting to the 2 mains methods `encode(...)` and `decode(...)`, we need to talk about `setHeader(...)` and `getHeader(...)`

There is a private value `__ready` set to `False`at *initialization* or each time we use one of the setters :

 - `setImageWidth(self, new_image_width)`
 - `setImageHeight(self, new_image_height)`
 - `setFPS(self, new_fps)`
> Side-Note : There are the relative getters `getImageHeight(self)`, `getImageWidth(self)`, `getFPS(self)` as well. We can access the values only via them to ensure we set ready to false at redefinition.

`ready` is used in `decode(...)` to be **sure we have fresh informations about the properties of the file**. 
For `ready` to be `True` again we need to call either :

 - **`__setHeader(self, video_time)`** : based on the provided `video_time` and the attributes it already has (from *initialization* or from *setters*), **rewrites the header in the file** such as described above in the file skeleton. **Put the file cursor ready to write frames**, and set `ready` to `True` 
 - **`__getHeader(self)`** : based on `filepath` provided at *initialization*, will **look at the header of the file** and accordingly **set all the properties** (`image_width`, `image_height`, `fps`, `framecount`) from it and set `ready` to `True`

### **`decode(self, frame_index=0, decoded_frame_count=-1, is_plotting=False)`**

Calls `getHeader(...)` if `ready` is `False`. And returns an array of **32x24 float64** frames if `is_plotting == False`, decoded from the file. **None** otherwise.

 - `frame_index` : the **index** of the **first frame** we wish to decode
 - `decoded_frame_count` : the **amount of frames** we want to decode
 - `is_plotting` : implies to visualize directly the data via *matplotlib* if `True`

### **`encode(self, video_time = 5, start_at = 0)`**

**Always calls** `setHeader(self, video_time)` to **make sure the header is well defined with fresh attributes** and the **file cursor is well positioned**. Start **capturing** all the frames at `at_start` time, **store them in RAM**, and **encode them directly after the header** in the file.
 - `video_time` : the **duration of the video** we wish to record (will define the `framecount` based on the `fps` attribute). Setting `video_time = 0` means taking a single photo.
 - `start_at` : the **OS time at which we want to start** recording (helps for synchronization later)

Many examples are provided in the python file to see how use it practically...
 
## Dual  Preview (`dual_preview.py`,`dual_preview_v2.py`)
Those files are simply **helpers to get a preview** of what you want to shot.
It captures live images with *4:3 format at 8 frames per second*, of **both thermal and visual cameras**. 
It's basically a simple *infinite loop* (that you can brake with **ctrl-C** or **q** respectively of the version, or simply by stopping the process) that is synchronized on cameras input write and read commands : when the cameras have finished shooting their images, the time to get the next ones will be defined by the framerate we imposed on them (here, 8 fps by default) 

 - The *first version* gives you a *matplotlib display* with **temperature colorbar**
 - The *second version* is made with *cv2*, it is **more reactive** and gives a **cleaner output**

## Dual Record (`dual_recorder.py`)
Details [here](https://docs.google.com/document/d/1zxq_STlwkWV-46vACSqwdSi-D5k8GLI7U22qNN7ov7M/edit?usp=sharing) on the cameras' specs. And also based on our meeting on acquisition [here](https://docs.google.com/document/d/1hH7w8HQmyj7rMtKnovtQBVmHZ_kZV0cLqmQQ3cjux8E/edit?usp=sharing).
This file is used to **record** dual content (**thermal** + **visual**).
It ***launches two subprocesses***, running content from *mlx_recorder.py* and
*picamera_recorder.py*.
The main variables to control execution are :
- `start_at` : particularly the value you add to the current time, it is
used to be sure the subprocesses are synchronized
- `video_time` : the duration you want to record

You will also see that there are other *important lines* like, the one where
we define :
- `th_filename` : you specify information that will lead to create the
*unique_id*, indicate the *framerate*, and the *video resolution*.
- `vi_filename` : you create the filename based on the first one to avoid
giving twice information, you can specify what you want to change on purpose though
More details about ***Data Naming Convention*** above.
*The point* is that **defining the filename also define properties of record**.

> Side-Note : Since I'm using subprocesses, I ensure the synchronization to the OS. This implied also that if one the subprocess fails it's hard to catch the error and make close the other ones. This produces errors, where child processes become orphelines and continue to exist until the video_time duration is elapsed... (especially with picamera library). If an error is catched the best is to do a keyboard-interrupt like ctrl-c (therefore running it in the terminal is more safe) accordingly to the little fix I tried to create (refers to `hardclean` methods in those files)


### Thermal Record (`mlx_recorder.py`)
File meant to be launched by *dual_recorder.py*
Starts a thermal record using MLXRawCodec based on the variables :
- `filename` : we talk about `th_filename` mentioned above, it gives the **main properties** to setup the codec via the method `detailsFromFilename` mentioned
- `start_at` and  `video_time` : such as *described above* as well
provided by command line.

### Visual Record (`picamera_recorder.py`)
File meant to be launched by dual_recorder.py
Starts a visual record using picamera based on the variables `filename` (this time analogous to `vi_filename`), `start_at` and  `video_time` in the exact same fashion as for thermal record.
> Side-Note : When the resolution is set to high, or the framerate is conjointly with resolution asking for a too high bitrate, the dual record can fail and break. Settings have to be experimentally verified.

> Side-Note 2 : After changing the resolution to 1440x1080 and approximately setting a bitrate (based on framerate and resolution), we get much more reliable results, in terms of crash avoiding and data weight scalability (before 8 minutes of splitted visual data = around 1 GB, now 5 minutes of continuous record = around 100 MB) 


## Example of Dual Record Read  (`dual_read_example.py`)
This file is meant to be an example on **how we use the data** we (or someone else) captured.
The important parts here is to show :
- how to **retrieve filename based on the unique id**
- how to **load the data** with **cv2** (for visual data) or **mlx_raw_codec** (for thermal data)

What is made with the data afterward is your job :D

> Side-note : More in depth, this file also serves as a viewer to visualize records, you can set the first frame index and the number of frame you want to visualize. It's highly inefficient and long when it is processed, but the visualization should be consistent



